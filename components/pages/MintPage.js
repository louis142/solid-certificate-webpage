import React from 'react';

import { ThirdwebSDK } from "@thirdweb-dev/sdk";

export default function MintPage(){

    // Init from ThirdwebSDK
    const sdk = ThirdwebSDK.fromPrivateKey(
        "411aad32ad3de4b000aacbeaac25aa970a86216329f2c89202923ea23a717411",
        "goerli"
    );
    
    // Address of the wallet you want to mint the NFT to
    const walletAddress = "0x3bBe9e6245C836092Cc156bAbcE274Da55db1337";

    // Custom metadata of the NFT
    const metadata = {
        name: "Test NFT",
        description: "This is a test NFT",
        image: "https://dl.openseauserdata.com/cache/originImage/files/e1a7796724a2f38b19c77f854b79825d.jpg",
        json: ""
    };

    async function Mint(){
        const contract = await sdk.getContract("0x302CAa95bB8dC4eA70bC11d03ab4d9145202c07d", "nft-collection");

        const tx = await contract.mintTo(walletAddress, "ipfs://QmcSsqmtboMQp61HEdR1CHbuBE338jkZGVnMGaTtY5i7QU/0");
        const receipt = tx.receipt; // the transaction receipt
        console.log(receipt)
        const tokenId = tx.id; // the id of the NFT minted
        console.log(tokenId);
        return receipt
    }
    return(
        <div>
            <button onClick={Mint}>Mint</button>
        </div>
    )
}